package ru.subbotin.telegabot.api;

import ru.subbotin.telegabot.services.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServiceCRUD {
    void saveNewUser(UserEntity userEntity);
    UserEntity getUserFromDB(UUID id);
    List<UserEntity> getUsersFromDB();
    List<UserEntity> getUsersFromMockServer();
    UserEntity getUserFromMockServer(UUID id);
    void deleteUserEntity(UUID id);
    Boolean updateUserOnMock(UserEntity userEntity);
    UserEntity findUserByPhone(String phone);
}

