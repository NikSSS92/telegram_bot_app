package ru.subbotin.telegabot.api;

import ru.subbotin.telegabot.services.UserEntity;

import java.util.List;
import java.util.UUID;

public interface MockClientOperations {
    UserEntity getUser(UUID id);
    List<UserEntity> getUsers();
    Boolean patchUser(UserEntity userEntity);
}
