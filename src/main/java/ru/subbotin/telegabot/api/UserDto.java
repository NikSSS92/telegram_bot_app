package ru.subbotin.telegabot.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String birthDay;
    private String chatId;
    private String firstName;
    private UUID id;
    private boolean isMale;
    private String middleName;
    private String phone;
    private String secondName;
}
