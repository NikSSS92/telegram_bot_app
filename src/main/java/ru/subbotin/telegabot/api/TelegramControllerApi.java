package ru.subbotin.telegabot.api;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramControllerApi {
    public SendMessage checkCallBackData(Update update);
    public SendMessage checkMessageData(Update update);
}
