package ru.subbotin.telegabot.services;

import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.subbotin.telegabot.api.MockClientOperations;
import ru.subbotin.telegabot.api.ServiceCRUD;
import ru.subbotin.telegabot.repository.UserCrudRepository;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class Services implements ServiceCRUD {

    //Сlass field
    private final MockClientOperations mockClient;

    private final UserCrudRepository userCrudRepository;
    //Сlass field


    //Actions on data in MockServer
    public Boolean updateUserOnMock(UserEntity userEntity) {
       return mockClient.patchUser(userEntity);
    }

    public UserEntity getUserFromMockServer(UUID id){
       return mockClient.getUser(id);
    }

    public List<UserEntity> getUsersFromMockServer(){
        return mockClient.getUsers();
    }
    //Actions on data in MockServer


    //Actions on data in database
    public void saveNewUser(UserEntity userEntity) {
        userCrudRepository.save(userEntity);
    }

    public List<UserEntity> getUsersFromDB(){
        final List<UserEntity> listUsers = Lists.newArrayList(userCrudRepository.findAll());
        return listUsers;
    }

    public UserEntity getUserFromDB(UUID id){
        return userCrudRepository.findById(id).get();
    }

    public void deleteUserEntity(UUID id){
        userCrudRepository.deleteById(id);
    }

    public UserEntity findUserByPhone(String phone){
        final List<UserEntity> listUsers = getUsersFromDB();
        return listUsers.stream().filter(m -> m.getPhone().equals(phone)).findAny().get();
    }
    //Actions on data in database

}

