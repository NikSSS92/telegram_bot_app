package ru.subbotin.telegabot.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import ru.subbotin.telegabot.api.*;
import ru.subbotin.telegabot.mappers.UserMapper;

import java.util.*;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class MockClient implements MockClientOperations {

    //Сlass field
    private final RestOperations restTemplate;

    private final UserMapper userMapper;

    @Value("${mockserver.url}")
    private String mockServerUrl;
    //Сlass field


    //Http headers
    private MultiValueMap<String, String> buildHttpHeaders() {   // create headers
        MultiValueMap<String, String> requestHeaders = new LinkedMultiValueMap<String, String>();
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        requestHeaders.add(HttpHeaders.HOST, "serene-coast-56441.herokuapp.com");
        requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        return requestHeaders;
    }
    //Http headers


    //Http requests methods
    public UserEntity getUser(UUID id) {

        HttpEntity<UserDto> headers = new HttpEntity<>(buildHttpHeaders());

        ResponseEntity<UserDto> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(mockServerUrl + "users/" + id, HttpMethod.GET, headers, UserDto.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
      //  final UserEntity userEntity = mapperUserDtoToUserEntity.apply(responseEntity.getBody());
        final UserEntity userEntity = userMapper.mapDtoToEntity(responseEntity.getBody());
        return userEntity;
    }

    public List<UserEntity> getUsers() {

        ResponseEntity<UserDto[]> responseEntity = null;

        try {
            responseEntity = restTemplate.getForEntity(mockServerUrl + "users", UserDto[].class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        final List<UserDto> listUsers = Arrays.asList(responseEntity.getBody());

        final List<UserEntity> listUsersEntity = userMapper.mapDtoListToUserEntitiesList(listUsers);

        return listUsersEntity;
    }

    public Boolean patchUser(UserEntity userEntity) {

        final UUID id = userEntity.getId();

        final UserUpdateDto userUpdateDto = mapperUserEntityToUpdateDto.apply(userEntity);

        HttpEntity<UserUpdateDto> headers = new HttpEntity<>(userUpdateDto, buildHttpHeaders());

        ResponseEntity<Boolean> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(mockServerUrl + "users/" + id, HttpMethod.PATCH, headers, Boolean.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return responseEntity.getBody();
    }

    //Http requests methods

    private Function<UserEntity, UserUpdateDto> mapperUserEntityToUpdateDto = userEntity -> UserUpdateDto.builder()
            .chatId(userEntity.getChatId())
            .phone(userEntity.getPhone())
            .birthDay(userEntity.getBirthDay())
            .build();



}
