package ru.subbotin.telegabot.services;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "telega_bot_users")
public class UserEntity {

    @Id
    private UUID id;

    @Column(name = "birth_day")
    private String birthDay;

    @Column(name = "chat_id")
    private String chatId;

    @Column(name = "first_Name")
    private String firstName;

    @Column(name = "is_male")
    private boolean isMale;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "second_name")
    private String secondName;

    @Override
    public String toString() {
        final String buildDate = this.birthDay.substring(8,10) + "-" + this.birthDay.substring(5,8) + this.birthDay.substring(0,4);
        return this.firstName + " " + this.secondName + " Birthday: " + buildDate + " Phone number: " + this.phone;
    }



}

