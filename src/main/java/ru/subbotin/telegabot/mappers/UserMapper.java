package ru.subbotin.telegabot.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.subbotin.telegabot.api.UserDto;
import ru.subbotin.telegabot.api.UserUpdateDto;
import ru.subbotin.telegabot.services.UserEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto mapEntityToDto(UserEntity entity);

    UserEntity mapDtoToEntity(UserDto dto);

    List<UserEntity> mapDtoListToUserEntitiesList(List<UserDto> dto);

    List<UserDto> mapEntitiesListToUserDtoList(List<UserEntity> entities);

    void updateUserDTO(@MappingTarget UserUpdateDto dto, UserEntity entity);

}
