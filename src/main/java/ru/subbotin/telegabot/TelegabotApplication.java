package ru.subbotin.telegabot;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Slf4j
@SpringBootApplication

public class TelegabotApplication {
    public static void main(String[] args) {

        ApiContextInitializer.init(); //Initialize the context

        SpringApplication.run(TelegabotApplication.class, args);
    }
}

