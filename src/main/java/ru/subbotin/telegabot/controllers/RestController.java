package ru.subbotin.telegabot.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import ru.subbotin.telegabot.api.UserDto;
import ru.subbotin.telegabot.api.ServiceCRUD;
import ru.subbotin.telegabot.mappers.UserMapper;
import ru.subbotin.telegabot.services.UserEntity;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/users")
@Api(value = "Account", description = "APIs for working with users in Telega bot")
@Component
@RequiredArgsConstructor
public class RestController {

    //Сlass field
    private final ServiceCRUD services;

    private final UserMapper userMapper;
    //Сlass field


    @GetMapping("/get/{userId}")
    @ApiOperation(value = "Get user by id from database")
    @ResponseStatus(code = HttpStatus.OK)
    public UserDto getUser(@PathVariable("userId") UUID id) {
        UserDto userDto = null;
        try {
            userDto = userMapper.mapEntityToDto(services.getUserFromDB(id));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return userDto;
    }

    @GetMapping("/get")
    @ApiOperation(value = "Get list of all user from database")
    @ResponseStatus(code = HttpStatus.OK)
    public HttpEntity<List<UserDto>> getUsers() {
        List<UserEntity> allUsersEntity = null;
        try {
            allUsersEntity = services.getUsersFromDB();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        List<UserDto> allUsersDto = userMapper.mapEntitiesListToUserDtoList(allUsersEntity);
        return ResponseEntity.ok(allUsersDto);
    }

    @DeleteMapping("/delete/{userId}")
    @ApiOperation(value = "Delete user from database by id")
    @ResponseStatus(code = HttpStatus.OK)
    public String deleteUser(@PathVariable("userId") UUID id) {
        try {
            services.deleteUserEntity(id);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return "User has been deleted";
    }

    @GetMapping("/phone/{phoneNumber}")
    @ApiOperation(value = "Find user user by phone number")
    @ResponseStatus(code = HttpStatus.OK)
    public HttpEntity<UserDto> searchUser(@PathVariable("phoneNumber") String phone) {
        UserDto userDto = null;
        try {
            userDto = userMapper.mapEntityToDto(services.findUserByPhone(phone));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(userDto);
    }

//    //    For debug
//    @GetMapping("/start")
//    @ApiOperation(value = "Debug request")
//    public void getUsersViaSevice() throws SQLException {
//    }
////    For debug

}
