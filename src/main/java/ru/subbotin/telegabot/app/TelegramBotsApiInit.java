package ru.subbotin.telegabot.app;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import ru.subbotin.telegabot.handlers.TelegaBotHandler;
import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class TelegramBotsApiInit {

    private final TelegaBotHandler telegaBotHandler;

    @Value("${application.userName}")
    private String USERNAME;

    @Value("${application.botToken}")
    private String TOKEN;


    @PostConstruct
    public void initTelegramBotsApi() {
        TelegramBotsApi botsApi = new TelegramBotsApi(); //Telegram Bots API
        try {
            telegaBotHandler.setTOKEN(TOKEN);
            telegaBotHandler.setUSERNAME(USERNAME);
            botsApi.registerBot(telegaBotHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
