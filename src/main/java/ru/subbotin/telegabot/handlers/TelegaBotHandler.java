package ru.subbotin.telegabot.handlers;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.subbotin.telegabot.api.ServiceCRUD;
import ru.subbotin.telegabot.api.UserDto;
import ru.subbotin.telegabot.services.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

@Slf4j
@Component
@RequiredArgsConstructor
public class TelegaBotHandler extends TelegramLongPollingBot {

    //Сlass field
    @Getter
    @Setter
    private String USERNAME;

    @Getter
    @Setter
    private String TOKEN;

    private final ServiceCRUD services;

    private static String codeCommand = "default";
    //Сlass field


    //TelegramLongPollingBot methods
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()) {
            SendMessage sendMessage = checkCallBackData(update);
            try {
                execute(sendMessage);
                log.info("Send from CallbackQuery branch");
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        } else if (update.hasMessage() && update.getMessage().hasText()) {
            SendMessage sendMessage = checkMessageData(update);

            try {
                execute(sendMessage); // Call method to send the message
                log.info("Send from message branch");
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }
    //TelegramLongPollingBot methods


    //Check data methods
    public SendMessage checkCallBackData(Update update) {

        long chatId = update.getCallbackQuery().getMessage().getChatId();

        SendMessage sendMessage = null;

        switch (update.getCallbackQuery().getData()) {
            case ("/addBirthday"): {
                codeCommand = "addBirthday";

                sendMessage = sendTextMessage("Введите дату рождения в формате дд-мм-гггг", chatId);
            }
            break;

            case ("/addPhoneNumber"): {
                codeCommand = "addPhoneNumber";

                sendMessage = sendTextMessage("Введите номер телефона", chatId);
            }
            break;

            case ("/users"): {
                if (services.getUsersFromDB().isEmpty()) {
                    sendMessage = sendTextMessage("Список пользователей пуст", chatId);
                } else {

                    final List<UserEntity> listUsers = services.getUsersFromDB();

                    sendMessage = sendTextMessage(listUsers.toString(), chatId);

                    System.out.println(listUsers.toString());

                }
            }
            break;
            default: {
                sendMessage = sendTextMessage("Неверная команда", chatId);
            }
            break;
        }
        return sendMessage;
    }

    public SendMessage checkMessageData(Update update) {

        long chatId = update.getMessage().getChatId();

        SendMessage sendMessage = null;

        if (codeCommand.equals("default")) {

            switch (update.getMessage().getText()) {
                case ("/start"): {
                    boolean userIsAlreadyRegistered = false;

                    if (!(services.getUsersFromDB().isEmpty())) { //The list of users in the bot's telegram storage is empty?
                        final List<UserEntity> listUsersEntityFromStorage = services.getUsersFromDB(); //Get all users from Telegram bot storage
                        userIsAlreadyRegistered = listUsersEntityFromStorage.stream().anyMatch(m -> m.getChatId().equals(Long.toString(chatId))); //If the user is in local storage, then true
                    }

                    if (userIsAlreadyRegistered) {
                        sendMessage = sendTextMessage("Вы уже зарегистрированы в чате", chatId);
                    } else {
                        sendMessage = singleButtonKeyboard(chatId, "Ввести номер телефона", "/addPhoneNumber", "Здравствуйте! Для регистрации в боте введите номер телефона");
                    }
                }
                break;

                default: {
                    sendMessage = mainKeyboard(chatId, "Cписок всех пользователей", "Обновить дату рождения", "/users", "/addBirthday", "Возможные команды: ", "Обновить номер телефона", "/addPhoneNumber");
                }
                break;
            }
        } else
            switch (codeCommand) {
                case ("addBirthday"): {
                    sendMessage = addingDataOfBirth(update);
                }
                break;
                case ("addPhoneNumber"): {
                    final List<UserEntity> listUsersEntityFromMockServer = services.getUsersFromMockServer(); //Get all users from Mock Server storage
                    final String phoneNumber = update.getMessage().getText();
                    boolean registrationApproved = false;

                    for (UserEntity userEntity : listUsersEntityFromMockServer) {
                        if (userEntity.getPhone() != null) {
                            if (userEntity.getPhone().equals(phoneNumber)) {
                                userEntity.setPhone(phoneNumber);
                                userEntity.setChatId(Long.toString(chatId));

                                services.saveNewUser(userEntity);

                                registrationApproved = services.updateUserOnMock(userEntity);
                            }
                        }
                    }

                    if (registrationApproved) { //If registration is allowed
                        sendMessage = sendTextMessage("Вы знесены в список пользователей", chatId);
                    } else {
                        sendMessage = sendTextMessage("Номер отсутствует в базе данных", chatId);
                    }

                    codeCommand = "default";

                }
                break;
                default: {
                    System.out.println("default");
                }
            }
        return sendMessage;
    }
    //Check data methods


    //Actions on data
    public SendMessage addingDataOfBirth(Update update) {
        final long chatId = update.getMessage().getChatId();

        final String dateOfBirth = update.getMessage().getText();
        final String chatIdString = update.getMessage().getChatId().toString();

        final List<UserEntity> listUsers = services.getUsersFromDB();

        boolean dateOfBirthSuccessfullyUpdated = false;

        SendMessage sendMessage = null;

        for (UserEntity userEntity : listUsers) {
            if (userEntity.getChatId().equals(chatIdString)) {
                userEntity.setBirthDay(reverseDataNumbers(dateOfBirth) + "T10:00:00.000+00:00");
                services.saveNewUser(userEntity);

                dateOfBirthSuccessfullyUpdated = services.updateUserOnMock(userEntity);
            }
        }

        codeCommand = "default";

        if (dateOfBirthSuccessfullyUpdated) {
            sendMessage = new SendMessage(chatId, "Дата рождения успешно обновлена");
        } else {
            sendMessage = new SendMessage(chatId, "Произошла ошибка");
        }
        return sendMessage;
}
    //Actions on data


    //Buttons
    public SendMessage singleButtonKeyboard(long chatId, String setText, String setCallbackData, String
            messageAfterPushOnButton) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        InlineKeyboardButton buttonAddBirthday = new InlineKeyboardButton();
        buttonAddBirthday.setText(setText);
        buttonAddBirthday.setCallbackData(setCallbackData);

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        rowInline.add(buttonAddBirthday);
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);

        return new SendMessage(chatId, messageAfterPushOnButton).setReplyMarkup(markupInline);
    }

    public SendMessage mainKeyboard(long chatId, String setText1, String setText2, String
            setCallbackData1, String setCallbackData2, String messageAfterPushOnButton, String setText3, String setCallbackData3) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        InlineKeyboardButton button1 = new InlineKeyboardButton();
        button1.setText(setText1);
        button1.setCallbackData(setCallbackData1);

        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button2.setText(setText2);
        button2.setCallbackData(setCallbackData2);

        InlineKeyboardButton button3 = new InlineKeyboardButton();
        button3.setText(setText3);
        button3.setCallbackData(setCallbackData3);

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        List<InlineKeyboardButton> row1Inline = new ArrayList<>(); //First line
        List<InlineKeyboardButton> row2Inline = new ArrayList<>(); //Second line

        row1Inline.add(button1);
        row1Inline.add(button2);

        row2Inline.add(button3);

        rowsInline.add(row1Inline);
        rowsInline.add(row2Inline);
        markupInline.setKeyboard(rowsInline);

        return new SendMessage(chatId, messageAfterPushOnButton).setReplyMarkup(markupInline);
    }
    //Buttons


    //Messages
    public SendMessage sendTextMessage(String textMessage, long chatId) {
        return new SendMessage().setText(textMessage).setChatId(chatId);
    }
    //Messages

    //Actions on symbol
    public String reverseDataNumbers(String string) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(string.substring(6, 10));
        stringBuilder.append(string.substring(2, 6));
        stringBuilder.append(string.substring(0, 2));
        return stringBuilder.toString();
    }
    //Actions on symbol

}

