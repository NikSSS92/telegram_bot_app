package ru.subbotin.telegabot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.subbotin.telegabot.services.UserEntity;

import java.util.UUID;

@Repository
public interface UserCrudRepository extends JpaRepository<UserEntity, UUID> {
}

//    List<User> getAllByChatId(String chatId);